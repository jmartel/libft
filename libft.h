/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 10:11:38 by jmartel           #+#    #+#             */
/*   Updated: 2020/01/19 03:45:34 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

/*
**malloc, free
*/
# include <stdlib.h>

/*
**read(2)
*/
# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>

/*
** stdarg : va_start, va_arg, va_copy, va_end
*/
# include <stdarg.h>

/*
** get_next_line
*/
# include <limits.h>

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

typedef struct		s_strsep
{
	char	**saved_start;
	char	*end;
	char	*head;
	char	save;
}					t_strsep;

typedef struct		s_dystr
{
	char			*str;
	int				len;
	int				size;
}					t_dystr;

/*
** get_next_line
*/

# define BUFF_SIZE 254
# define GNL_MAX_FD	OPEN_MAX

/*
** NB : types priority in files : di, xo, u, f, p,  s, c
** When needed you will find di, x, o, u, f in the first file
** p, s, c and % are in the printf_XXXX2.c
*/

/*
** Definition of a char used to represent non conversion type (text only)
*/
# define TYPE_TEXT '$'

/*
**Definition of values used when precision or size are not precised
*/
# define NO_PREC -1
# define NO_SIZE -1

/*
** Bitewises associated to different flags
*/
# define CAST_H		0x0001
# define CAST_HH	0x0002
# define CAST_L		0x0004
# define CAST_LL	0x0008

# define FLAG_PLUS	0x0010
# define FLAG_MINUS	0x0020
# define FLAG_ZERO	0x0040
# define FLAG_HASH	0x0080
# define FLAG_SPACE	0x0100

/*
** ANSI color constants
*/

# define RED        "\x1B[31m"
# define GREEN      "\x1B[32m"
# define YELLOW     "\x1B[33m"
# define BLUE       "\x1B[34m"
# define MAGENTA    "\x1B[35m"
# define CYAN       "\x1B[36m"
# define WHITE      "\x1B[37m"
# define L_GREY     "\x1B[90m"
# define L_BLUE     "\x1B[94m"
# define L_MAGENTA  "\x1B[95m"
# define L_CYAN     "\x1B[96m"
# define L_GREEN    "\e[1;32m"
# define BRED       "\x1B[41m"
# define BGREEN     "\x1B[42m"
# define BYELLOW    "\x1B[43m"
# define BBLUE      "\x1B[44m"
# define BMAGENTA   "\x1B[44m"
# define BCYAN      "\x1B[46m"
# define BWHITE     "\x1B[47m"
# define BL_BLUE    "\x1B[104m"
# define BL_MAGENTA "\x1B[105m"
# define BL_CYAN    "\x1B[106m"
# define BOLD       "\x1b[1m"
# define UNDERLINE  "\x1b[4m"
# define EOC        "\033[0m"

/*
** s_conv struct is used to represent any part of the format input string
** - type is used to stock the type of conversion
** - conv : stock the flags given as bytes,  defined by sub-macros
** - size : stock the size given, NO_SIZE if not given
** - prec : stock the precision given, NO_PREC if not given
** - res : stock the result of the conversion as a string
** - next : stock the adress of next link
*/
typedef struct		s_conv
{
	char		type;
	int			conv;
	int			size;
	int			prec;
	char		*res;
	void		*next;
}					t_conv;

/*
********************************************************************************
*/

/*
** atoi/ft_atoi.c
*/
int					ft_atoi(const char *str);

/*
** atoi/ft_ftoa.c
*/
char				*ft_ftoa(long double f, int prec);

/*
** atoi/ft_itoa.c
*/
char				*ft_itoa(int n);

/*
** atoi/ft_lltoa.c
*/
char				*ft_lltoa(long long l, int base);

/*
** atoi/ft_ltoa.c
*/
char				*ft_ltoa(long l, int base);

/*
** atoi/ft_ulltoa.c
*/
char				*ft_ulltoa(unsigned long long l, int base);

/*
** dystr/ft_dystr_check.c
*/
t_dystr				*ft_dystr_check(t_dystr *dystr, int new_offset);

/*
** dystr/ft_dystr_free.c
*/
void				ft_dystr_free(t_dystr *dystr);

/*
** dystr/ft_dystr_new.c
*/
t_dystr				*ft_dystr_new(char *str, size_t size, size_t len);

/*
** dystr/ft_dystr_realloc.c
*/
t_dystr				*ft_dystr_realloc(t_dystr *dystr);

/*
** gnl/get_next_line.c
*/
int					get_next_line(const int fd, char **line);

/*
** lst/ft_lstadd.c
*/
void				ft_lstadd(t_list **alst, t_list *new);

/*
** lst/ft_lstadd_last.c
*/
void				ft_lstadd_last(t_list **start, t_list *new);

/*
** lst/ft_lstdel.c
*/
void				ft_lstdel(t_list **alst, void (*del)(void*, size_t));

/*
** lst/ft_lstdelone.c
*/
void				ft_lstdelone(
	t_list **alst, void (*del)(void *, size_t));

/*
** lst/ft_lstiter.c
*/
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));

/*
** lst/ft_lstlen.c
*/
size_t				ft_lstlen(t_list *start);

/*
** lst/ft_lstmap.c
*/
t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));

/*
** lst/ft_lstnew.c
*/
t_list				*ft_lstnew(void const *content, size_t content_size);

/*
** lst/ft_lstput_fd.c
*/
void				ft_lstput_fd(t_list *start, int fd);

/*
** math/ft_abs.c
*/
int					ft_abs(int a);

/*
** math/ft_longlen.c
*/
size_t				ft_longlen(long nb);

/*
** math/ft_max.c
*/
int					ft_max(int a, int b);

/*
** math/ft_min.c
*/
int					ft_min(int a, int b);

/*
** math/ft_pow.c
*/
long				ft_pow(long x, long y);

/*
** math/ft_roundf.c
*/

/*
** math/ft_roundl.c
*/
long double			ft_roundl(long double x);

/*
** mem/ft_byteprint.c
*/
void				byteprint(void *ptr);
void				byteprintn(void *ptr);
void				byteprinthex(void *ptr);
void				byteprinthexn(void *ptr);

/*
** mem/ft_bzero.c
*/
void				ft_bzero(void *s, size_t n);

/*
** mem/ft_hton.c
*/

/*
** mem/ft_memalloc.c
*/
void				*ft_memalloc(size_t size);

/*
** mem/ft_memccpy.c
*/
void				*ft_memccpy(
	void *dst, const void *src, int c, size_t len);

/*
** mem/ft_memchr.c
*/
void				*ft_memchr(const void *s, int c, size_t n);

/*
** mem/ft_memcmp.c
*/
int					ft_memcmp(const void *s1, const void *s2, size_t n);

/*
** mem/ft_memcpy.c
*/
void				*ft_memcpy(void *dst, const void *src, size_t n);

/*
** mem/ft_memdel.c
*/
void				ft_memdel(void **ap);

/*
** mem/ft_memmove.c
*/
void				*ft_memmove(void *dst, const void *src, size_t len);

/*
** mem/ft_memprint.c
*/
void				byte_block_to_bin_str(void *ptr, int n, char *b);
void				byte_block_to_hex_str(void *ptr, int n, char *b);
void				byte_block_to_ascii_str(void *ptr, int n, char *b);
void				dump_packet(void *ptr, int n);
int					main(void);

/*
** mem/ft_memset.c
*/
void				*ft_memset(void *b, int c, size_t len);

/*
** printf/ft_asprintf.c
*/
char				*ft_asprintf(const char *format, ...);
char				*ft_asprintf_result(t_conv *start);

/*
** printf/ft_dprintf.c
*/
int					ft_dprintf(int fd, const char *format, ...);

/*
** printf/ft_printf.c
*/
int					ft_printf(const char *format, ...);
int					ft_printf_args(t_conv *head, va_list va);

/*
** printf/ft_printf_cast.c
*/
void				cast_di(va_list va, t_conv *conv);
unsigned long long	cast_xo(va_list va, t_conv *conv);
unsigned long long	cast_u(va_list va, t_conv *conv);
void				cast_f(va_list va, t_conv *conv);

/*
** printf/ft_printf_conv.c
*/
int					t_conv_len(const char *restrict format);
int					t_conv_new(t_conv *conv, const char *str);
void				t_conv_size_prec(char *str, t_conv *conv);
int					t_conv_detect_zero(char *str);
void				t_conv_free_lst(t_conv *start);

/*
** printf/ft_printf_conv_is.c
*/
int					conv_isconv(char c);

/*
** printf/ft_printf_fill.c
*/
void				fill_di(va_list va, t_conv *conv);
void				fill_x(va_list va, t_conv *conv);
void				fill_o(va_list va, t_conv *conv);
void				fill_u(va_list va, t_conv *conv);
void				fill_f(va_list va, t_conv *conv);

/*
** printf/ft_printf_fill2.c
*/
void				fill_p(va_list va, t_conv *conv);
void				fill_s(va_list va, t_conv *conv);
void				fill_c(va_list va, t_conv *conv);
void				fill_perc(t_conv *conv);
void				fill_b(va_list va, t_conv *conv);

/*
** printf/ft_printf_flag.c
*/
void				flag_plus(t_conv *conv, int sign);
void				flag_space(t_conv *conv, int sign);

/*
** printf/ft_printf_flag_hash.c
*/
void				flag_hash_x(t_conv *conv);
void				flag_hash_o(t_conv *conv);
void				flag_hash_f(t_conv *conv);

/*
** printf/ft_printf_flag_prec.c
*/
void				flag_prec_di(t_conv *conv);
void				flag_prec_x(t_conv *conv);
void				flag_prec_o(t_conv *conv);
void				flag_prec_u(t_conv *conv);

/*
** printf/ft_printf_flag_prec2.c
*/
void				flag_prec_s(t_conv *conv);

/*
** printf/ft_printf_flag_size.c
*/
void				flag_size_di(t_conv *conv);
void				flag_size_x(t_conv *conv);
void				flag_size_o(t_conv *conv);
void				flag_size_u(t_conv *conv);
void				flag_size_f(t_conv *conv);

/*
** printf/ft_printf_flag_size2.c
*/
void				flag_size_p(t_conv *conv);
void				flag_size_s(t_conv *conv);
void				flag_size_c(t_conv *conv);
void				flag_size_perc(t_conv *conv);

/*
** printf/ft_printf_put.c
*/
int					ft_printf_put_result(t_conv *head);
int					ft_dprintf_put_result(int fd, t_conv *head);

/*
** printf/ft_printf_split.c
*/
t_conv				*ft_printf_split(const char *format);
t_conv				*ft_printf_split_push_conv(
	t_conv **start, t_conv *new);
void				ft_printf_split_free_list(t_conv *start);
t_conv				*ft_printf_split_new_txt(char *str);
t_conv				*ft_printf_split_new_conv(char *str);

/*
** put/ft_putchar.c
*/
void				ft_putchar(char c);

/*
** put/ft_putchar_fd.c
*/
void				ft_putchar_fd(char c, int fd);

/*
** put/ft_putcharn.c
*/
void				ft_putcharn(char c);

/*
** put/ft_putendl.c
*/
void				ft_putendl(char const *s);

/*
** put/ft_putendl_fd.c
*/
void				ft_putendl_fd(char const *s, int fd);

/*
** put/ft_putnbr.c
*/
void				ft_putnbr(int n);

/*
** put/ft_putnbr_fd.c
*/
void				ft_putnbr_fd(int n, int fd);

/*
** put/ft_putnbrn.c
*/
void				ft_putnbrn(int nb);

/*
** put/ft_putstr.c
*/
void				ft_putstr(char const *s);

/*
** put/ft_putstr_fd.c
*/
void				ft_putstr_fd(char const *s, int fd);

/*
** put/ft_putstrn.c
*/
void				ft_putstrn(char *str);

/*
** put/ft_puttab_int.c
*/
void				ft_puttab_int(int *tab, int size);

/*
** str/ft_chpbrk.c
*/
int					ft_chpbrk(const char c, const char *charset);

/*
** str/ft_isalnum.c
*/
int					ft_isalnum(int c);

/*
** str/ft_isalnum_only.c
*/
int					ft_isalnum_only(char *str);

/*
** str/ft_isalpha.c
*/
int					ft_isalpha(int c);

/*
** str/ft_isalpha_only.c
*/
int					ft_isalpha_only(char *str);

/*
** str/ft_isascii.c
*/
int					ft_isascii(int c);

/*
** str/ft_isascii_only.c
*/
int					ft_isascii_only(char *str);

/*
** str/ft_isdigit.c
*/
int					ft_isdigit(int c);

/*
** str/ft_isdigit_only.c
*/
int					ft_isdigit_only(char *str);

/*
** str/ft_isprint.c
*/
int					ft_isprint(int c);

/*
** str/ft_isprint_only.c
*/
int					ft_isprint_only(char *str);

/*
** str/ft_isseparator.c
*/
int					ft_isseparator(int c);

/*
** str/ft_iswhite.c
*/
int					ft_iswhite(char c);

/*
** str/ft_iswhite_only.c
*/
int					ft_iswhite_only(char *str);

/*
** str/ft_split_whitespaces.c
*/
char				**ft_split_whitespaces(char *str);

/*
** str/ft_strcat.c
*/
char				*ft_strcat(char *s1, const char *s2);

/*
** str/ft_strchr.c
*/
char				*ft_strchr(const char *s, int c);

/*
** str/ft_strclr.c
*/
void				ft_strclr(char *s);

/*
** str/ft_strcmp.c
*/
int					ft_strcmp(const char *s1, const char *s2);

/*
** str/ft_strcontains.c
*/
int					ft_strcontains(char *charset, char c);

/*
** str/ft_strcpy.c
*/
char				*ft_strcpy(char *dest, const char *src);

/*
** str/ft_strdel.c
*/
void				ft_strdel(char **as);

/*
** str/ft_strdelchar.c
*/
void				ft_strdelchar(char *str, int index);
void				ft_strdelchars(char *str, int index, int len);

/*
** str/ft_strdup.c
*/
char				*ft_strdup(const char *s1);

/*
** str/ft_strequ.c
*/
int					ft_strequ(const char *s1, const char *s2);

/*
** str/ft_strichr.c
*/
int					ft_strichr(const char *s, int c);

/*
** str/ft_strichr_last.c
*/
int					ft_strichr_last(const char *s, int c);

/*
** str/ft_strinsert_free.c
*/
char				*ft_strinsert_free(
	char *s1, char *s2, int pos, int param);

/*
** str/ft_striter.c
*/
void				ft_striter(char *s, void (*f)(char *));

/*
** str/ft_striteri.c
*/
void				ft_striteri(char *s, void (*f)(unsigned int, char *));

/*
** str/ft_strjoin.c
*/
char				*ft_strjoin(const char *s1, const char *s2);

/*
** str/ft_strjoin_3.c
*/
char				*ft_strjoin_3(
	char const *s1, char const *s2, char const *s3);

/*
** str/ft_strjoin_3_free.c
*/
char				*ft_strjoin_3_free(
	char const *s1, char const *s2, char const *s3);

/*
** str/ft_strjoin_free.c
*/
char				*ft_strjoin_free(
	const char *s1, const char *s2, int param);

/*
** str/ft_strjoin_path.c
*/
char				*ft_strjoin_path_free(char *s1, char *s2, int opt);
char				*ft_strjoin_path(char *s1, char *s2);

/*
** str/ft_strlcat.c
*/
size_t				ft_strlcat(char *dst, const char *src, size_t size);

/*
** str/ft_strlen.c
*/
size_t				ft_strlen(const char *s);

/*
** str/ft_strmap.c
*/
char				*ft_strmap(char const *s, char (*f)(char));

/*
** str/ft_strmapi.c
*/
char				*ft_strmapi(char *s, char (*f)(unsigned int, char));

/*
** str/ft_strncat.c
*/
char				*ft_strncat(char *s1, const char *s2, size_t n);

/*
** str/ft_strncmp.c
*/
int					ft_strncmp(const char *s1, const char *s2, size_t n);

/*
** str/ft_strncpy.c
*/
char				*ft_strncpy(char *dest, const char *src, size_t n);

/*
** str/ft_strndup.c
*/
char				*ft_strndup(const char *str, size_t n);

/*
** str/ft_strnequ.c
*/
int					ft_strnequ(const char *s1, const char *s2, size_t n);

/*
** str/ft_strnew.c
*/
char				*ft_strnew(size_t size);

/*
** str/ft_strninsert_free.c
*/
int					ft_strninsert_free(
	char **str, int *pos, char c, int nb);

/*
** str/ft_strnjoin_free.c
*/
char				*ft_strnjoin_free(char *s1, char *s2, size_t n);

/*
** str/ft_strnlen.c
*/
size_t				ft_strnlen(const char *s, size_t maxlen);

/*
** str/ft_strnrest.c
*/
char				*ft_strnrest(char *str, int n);

/*
** str/ft_strnstr.c
*/
char				*ft_strnstr(
	const char *str, const char *find, size_t len);
char				*ft_strrnstr(
	const char *str, const char *find, size_t len);

/*
** str/ft_strpbrk.c
*/
char				*ft_strpbrk(const char *s, const char *charset);

/*
** str/ft_strrchr.c
*/
char				*ft_strrchr(const char *s, int c);

/*
** str/ft_strrep_free.c
*/
char				*ft_strrep(char *s1, char *s2, int start, int len);
char				*ft_strrep_free(
	char *s1, char *s2, int start, int len);
char				*ft_strrep_pattern_free(
	char *s1, char *s2, char *pattern, int param);

/*
** str/ft_strrev.c
*/
void				ft_strrev(char *s);

/*
** str/ft_strsep.c
*/
char				*ft_strsep(char **original_start, char *delim);

/*
** str/ft_strsplit.c
*/
char				**ft_strsplit(char const *s, char c);

/*
** str/ft_strstr.c
*/
char				*ft_strstr(const char *str, const char *find);
char				*ft_strrstr(const char *str, const char *find);

/*
** str/ft_strstr_count.c
*/
int					ft_strstr_count(char *str, char *pattern);

/*
** str/ft_strsub.c
*/
char				*ft_strsub(
	const char *s, unsigned int start, size_t len);

/*
** str/ft_strtab_free.c
*/
void				ft_strtab_free(char ***tab);

/*
** str/ft_strtab_len.c
*/
int					ft_strtab_len(char **tab);

/*
** str/ft_strtab_new.c
*/
char				**ft_strtab_new(int x, int y);

/*
** str/ft_strtab_new_line.c
*/
char				**ft_strtab_new_line(char **tab, int free);

/*
** str/ft_strtab_put.c
*/
void				ft_strtab_put(char **tab);

/*
** str/ft_strtok.c
*/
char				*ft_strtok(char *str, char *delim);

/*
** str/ft_strtolower.c
*/
char				*ft_strtolower(char *str);

/*
** str/ft_strtrim.c
*/
char				*ft_strtrim(char const *s);

/*
** str/ft_strtrim_side.c
*/
char				*ft_strtrim_side(char *str, char *charset, int side);

/*
** str/ft_substitute_str.c
*/
int					ft_substitute_str(
	char **str, char *to_inject, int index_to_inject, int len);

/*
** str/ft_tolower.c
*/
int					ft_tolower(int c);

/*
** str/ft_toupper.c
*/
int					ft_toupper(int c);

#endif

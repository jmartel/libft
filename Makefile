# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/04/11 23:08:04 by jmartel           #+#    #+#              #
#    Updated: 2021/05/24 17:04:47 by jmartel          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		= libft.a

debug		?= 0
ANALYZE		?= 0
SPEED		= -j8

SRCDIR		= srcs
OBJDIR		= objs
INCLUDESDIR = .

CC			= gcc
CFLAGS		= $(INC) -Wall -Werror -Wextra

VPATH		= 	$(INCLUDESDIR) \
				$(SRCDIR)/atoi \
				$(SRCDIR)/mem \
				$(SRCDIR)/gnl \
				$(SRCDIR)/lst \
				$(SRCDIR)/str \
				$(SRCDIR)/math \
				$(SRCDIR)/put \
				$(SRCDIR)/dystr \
				$(SRCDIR)/printf

SRCS		=	ft_atoi.c ft_ftoa.c ft_itoa.c ft_lltoa.c ft_ltoa.c ft_ulltoa.c 

SRCS		+=	ft_lstadd.c ft_lstadd_last.c ft_lstdel.c ft_lstdelone.c \
				ft_lstiter.c ft_lstlen.c ft_lstmap.c ft_lstnew.c ft_lstput_fd.c 

SRCS		+=	ft_abs.c ft_longlen.c ft_max.c ft_min.c ft_pow.c ft_roundf.c ft_roundl.c

SRCS		+=	ft_bzero.c ft_memalloc.c ft_memccpy.c ft_memchr.c ft_memcmp.c \
				ft_memcpy.c ft_memdel.c ft_memmove.c ft_memset.c ft_byteprint.c  \
				ft_hton.c ft_memprint.c

SRCS		+=	ft_putchar.c ft_putchar_fd.c ft_putcharn.c ft_putendl.c \
				ft_putendl_fd.c ft_putnbr.c ft_putnbr_fd.c ft_putnbrn.c \
				ft_putstr.c ft_putstr_fd.c ft_putstrn.c ft_puttab_int.c

################################################################################
########							STR									########
################################################################################
SRCS		+=	ft_chpbrk.c ft_isalnum.c ft_isalnum_only.c ft_isalpha.c ft_isalpha_only.c ft_isascii.c \
				ft_isascii_only.c ft_isdigit.c ft_isdigit_only.c ft_isprint.c \
				ft_isprint_only.c ft_isseparator.c ft_iswhite.c ft_iswhite_only.c \
				ft_split_whitespaces.c ft_strcat.c ft_strchr.c ft_strclr.c ft_strcmp.c ft_strcontains.c \
				ft_strcpy.c ft_strdel.c ft_strdelchar.c ft_strdup.c ft_strequ.c ft_strichr.c \
				ft_strichr_last.c ft_strinsert_free.c ft_striter.c ft_striteri.c ft_strjoin.c \
				ft_strjoin_3.c ft_strjoin_3_free.c ft_strjoin_free.c ft_strjoin_path.c ft_strlcat.c \
				ft_strlen.c ft_strmap.c ft_strmapi.c ft_strncat.c ft_strncmp.c ft_strncpy.c \
				ft_strndup.c ft_strnequ.c ft_strnew.c ft_strninsert_free.c ft_strnjoin_free.c \
				ft_strnlen.c ft_strnrest.c ft_strnstr.c ft_strpbrk.c ft_strrchr.c ft_strrep_free.c \
				ft_strrev.c ft_strsep.c ft_strsplit.c ft_strstr.c ft_strstr_count.c ft_strsub.c\
				ft_strtab_new.c ft_strtab_free.c ft_strtab_len.c  ft_strtab_new_line.c ft_strtab_put.c \
				ft_strtok.c \
				ft_strtolower.c ft_strtrim.c ft_strtrim_side.c ft_substitute_str.c ft_tolower.c ft_toupper.c 

################################################################################
########							DYSTR								########
################################################################################
SRCS		+=	ft_dystr_check.c ft_dystr_free.c ft_dystr_new.c ft_dystr_realloc.c \

################################################################################
########							PRINTF								########
################################################################################
SRCS		+=	ft_asprintf.c ft_dprintf.c ft_printf.c ft_printf_cast.c ft_printf_conv.c \
				ft_printf_conv_is.c ft_printf_fill.c ft_printf_fill2.c ft_printf_flag.c \
				ft_printf_flag_hash.c ft_printf_flag_prec.c ft_printf_flag_prec2.c ft_printf_flag_size.c \
				ft_printf_flag_size2.c ft_printf_put.c ft_printf_split.c

SRCS		+=	get_next_line.c

OBJECTS		=	$(addprefix $(OBJDIR)/, $(SRCS:.c=.o))
INC 		=	-I $(INCLUDESDIR)

EOC			= \033[0m
OK_COLOR	= \x1b[32;01m
FLAGS_COLOR	= \x1b[34;01m

ifeq ($(DEBUG), 1)
	CFLAGS += -fsanitize=address
	CC += -g3
endif

ifeq ($(ANALYZE), 1)
	CFLAGS += analyze
endif

all:
	@echo "$(FLAGS_COLOR)Compiling with flags $(CFLAGS) $(EOC)"
	@make -C . header
	@mkdir -p $(OBJDIR)
	@$(MAKE) $(NAME) $(SPEED)

header:
	@python3 .libft_header.py

debug:
	@$(MAKE) all DEBUG=1

analyze:
	@$(MAKE) all ANALYZE=1

$(NAME): $(OBJDIR) $(OBJECTS)
	@ar -rcs $(NAME) $(OBJECTS)
	@echo "$(OK_COLOR)$(NAME) linked with success !$(EOC)"

$(OBJDIR)/%.o: $(SRC_DIR)%.c $(INCLUDES)
	@$(CC) -c $< -o $@ $(CFLAGS)
	@echo "${COMP_COLOR}$< ${EOC}"

clean:
	@$(RM) $(OBJECTS)
	@$(RM) -r $(OBJDIR) && echo "${OK_COLOR}Successfully cleaned $(NAME) objects files ${EOC}"

fclean: clean
	@$(RM) $(NAME)  && echo "${OK_COLOR}Successfully cleaned $(NAME) ${EOC}"

re: fclean all

.PHONY: all debug analyze clean fclean re rere

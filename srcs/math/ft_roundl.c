/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_roundl.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/28 15:50:49 by jmartel           #+#    #+#             */
/*   Updated: 2019/11/28 15:50:49 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

long double		ft_roundl(long double x)
{
	long long int		nb;

	if (x < 0)
		x -= 0.5;
	else
		x += 0.5;
	nb = (long long int)x;
	return ((long double)nb);
}

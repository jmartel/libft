/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtab_free.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 20:42:05 by jmartel           #+#    #+#             */
/*   Updated: 2020/01/19 04:48:44 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_strtab_free(char ***tab)
{
	int			i;

	if (!tab || !*tab)
		return ;
	i = 0;
	while ((*tab)[i])
	{
		ft_strdel((*tab) + i);
		i++;
	}
	ft_memdel((void**)tab);
	*tab = NULL;
	return ;
}

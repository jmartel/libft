/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnjoin_free.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/28 15:49:16 by jmartel           #+#    #+#             */
/*   Updated: 2019/11/28 15:49:17 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnjoin_free(char *s1, char *s2, size_t n)
{
	char *res;

	if (s1 == NULL || s2 == NULL)
		return (NULL);
	if (!(res = ft_strnew(ft_strlen(s1) + ft_min(ft_strlen(s2), n) + 1)))
	{
		ft_strdel(&s1);
		return (NULL);
	}
	ft_strclr(res);
	ft_strcat(res, s1);
	ft_strncat(res, s2, n);
	ft_strdel(&s1);
	return (res);
}

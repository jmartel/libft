/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtok.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/28 15:49:35 by jmartel           #+#    #+#             */
/*   Updated: 2019/11/28 15:49:35 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strtok(char *str, char *delim)
{
	static char		*buff;
	char			*buff_it;
	char			*token;

	if (str)
		buff = str;
	while (*buff && ft_strcontains(delim, *buff))
		buff++;
	if (*buff == '\0')
		return (NULL);
	token = buff;
	buff_it = buff;
	while (*buff_it)
	{
		if (ft_strcontains(delim, *buff_it))
		{
			*buff_it = '\0';
			buff = buff_it + 1;
			return (token);
		}
		buff_it++;
	}
	buff = buff_it;
	return (token);
}

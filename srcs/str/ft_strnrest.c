/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnrest.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/28 15:47:30 by jmartel           #+#    #+#             */
/*   Updated: 2019/11/28 15:47:30 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnrest(char *str, int n)
{
	char	*res;
	int		len;

	len = ft_strlen(str);
	if (!(res = ft_strndup(&str[n], len - n)))
		return (NULL);
	return (res);
}

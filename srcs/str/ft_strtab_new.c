/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtab_new.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/13 00:01:26 by jmartel           #+#    #+#             */
/*   Updated: 2020/01/17 17:30:01 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		**ft_strtab_new(int x, int y)
{
	char	**tab;
	int		i;

	tab = malloc(sizeof(*tab) * (x + 1));
	if (!tab)
		return (NULL);
	i = 0;
	while (i < x)
	{
		tab[i] = ft_memalloc(sizeof(**tab) * (y + 1));
		if (!tab[i])
		{
			while (--i >= 0)
				free(tab[i]);
			free(tab);
			return (NULL);
		}
		i++;
	}
	tab[i] = NULL;
	return (tab);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strichr_last.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/28 15:47:44 by jmartel           #+#    #+#             */
/*   Updated: 2019/11/28 15:47:46 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strichr_last(const char *s, int c)
{
	int i;
	int index;

	index = -1;
	i = 0;
	while (s[i])
	{
		if (s[i] == c)
			index = i;
		i++;
	}
	return (index);
}

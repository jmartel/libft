/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_byteprint.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/24 13:37:52 by jmartel           #+#    #+#             */
/*   Updated: 2021/05/24 14:48:10 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

static void	bytetobin(void *ptr, char *b)
{
	int		i;
	char	*p;

	i = 0;
	p = (char*)ptr;
	while (i < 8)
	{
		b[i] = (*p >> (7-i)) & 1 ? '1' : '0';
		i++;
	}
}

void		byteprint(void *ptr)
{
	char	b[8];

	bytetobin(ptr, b);
	write(1, b, 8);
}

void		byteprintn(void *ptr)
{
	char	b[9];

	bytetobin(ptr, b);
	b[8] = '\n';
	write(1, b, 9);
}

static void	bytetohex(void *ptr, char *b)
{
	static char	s[] = "0123456789abcdef";
	char		*p;

	p = (char*)ptr;
	b[0] = '0';
	b[1] = 'x';
	b[2] = s[(*p >> 4) & 15];
	b[3] = s[*p & 15];
}

void		byteprinthex(void *ptr)
{
	char		b[4];

	bytetohex(ptr, b);
	write(1, b, 4);
}

void		byteprinthexn(void *ptr)
{
	char		b[5];

	bytetohex(ptr, b);
	b[4] = '\n';
	write(1, b, 5);
}

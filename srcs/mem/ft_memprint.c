/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memprint.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/24 14:44:50 by jmartel           #+#    #+#             */
/*   Updated: 2021/05/24 17:00:08 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

/*
** n is number of bytes to read
** b shall be at least 9 * n long (block will be space split)
*/
void		byte_block_to_bin_str(void *ptr, int n, char *b)
{
	int		i;
	int		j;
	int		t;
	char	*p;

	i = 0;
	t = 0;
	p = (char*)ptr;
	while (i < n)
	{
		j = 0;
		while (j < 8)
		{
			b[t] = (p[i] >> (7-j)) & 1 ? '1' : '0';
			t++;
			j++;
		}
		if (i < n - 1)
		{
			b[t] = ' ';
			t++;
		}
		i++;
	}
	b[t] = '\0';
}

/*
** n is number of bytes to read
** b shall be at least 3 * (n - 1) + 2 * n + 1 long (block will be space split)
*/
void		byte_block_to_hex_str(void *ptr, int n, char *b)
{
	int		i;
	int		j;
	char	*p;
	static char	s[] = "0123456789abcdef";

	i = 0;
	p = (char*)ptr;
	while (i < n)
	{
		j = 0;
		while (j < 8)
		{
			b[3 * i] = s[p[i] >> 4];
			b[3 * i + 1] = s[p[i] & 15];
			j++;
		}
		i++;
		if (i < n)
			b[3 * i - 1] = ' ';
	}
	b[3 * i] = '\0';
}

/*
** n is number of bytes to read
** b shall be at least n + 1 long
*/
void		byte_block_to_ascii_str(void *ptr, int n, char *b)
{
	int		i;
	char	*p;

	i = 0;
	p = (char*)ptr;
	while (i < n)
	{
		if (p[i] >= 32 && p[i] <= 126)
			b[i] = p[i];
		else
			 b[i] = '.';
		i++;
	}
	b[i] = '\0';
}

void		dump_packet(void *ptr, int n)
{
	char	buffer[16 * 9];
	int		i;
	int		block_len;

	i = 0;
	while (i < n / 16)
	{
		byte_block_to_hex_str(ptr + 16*i, 16, buffer);
		write(1, buffer, 15 * 3 + 2);
		write(1, "    ", 4);
		byte_block_to_ascii_str(ptr + 16*i, 16, buffer);
		write(1, buffer, 16);
		write(1, "\n", 1);
		i++;
	}
	block_len = n % 16;
	if (block_len != 0)
	{
		byte_block_to_hex_str(ptr + 16*i, block_len, buffer);
		write(1, buffer, block_len * 3);
		write(1, "    ", 4);
		byte_block_to_ascii_str(ptr + 16*i, block_len, buffer);
		write(1, buffer, block_len);
		write(1, "\n", 1);
	}
}

int		main(void)
{
	char	*s;

	s = "0123456789abcdeffedcba9876543210";

	dump_packet(s, 32);
	return (0);
}

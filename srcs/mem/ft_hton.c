/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hton.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/24 16:15:59 by jmartel           #+#    #+#             */
/*   Updated: 2021/05/24 16:42:36 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>

static	int		is_bigendian()
{
	int		i;

	i = 0x00000001;
	return (char*)(&i) == 0;
}

uint16_t		htons(uint16_t hostlong)
{
	if (is_bigendian())
		return hostlong;
	return (hostlong >> 8 | hostlong << 8);
}

uint32_t		htonl(uint32_t hostshort)
{
	if (is_bigendian())
		return hostshort;
	return (htons((uint16_t)hostshort) << 16 | htons(hostshort >> 16));
}
